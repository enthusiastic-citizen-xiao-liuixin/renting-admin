import axios from "axios";
import { store } from "../store/index";

/*  配置请求拦截器  */

// 创建对象 
const Server = axios.create({
    // 基地址
    // baseURL: 'http://api.xiaohuihui0728.cn:8888/api/private/v1/', 
    // timeout: 5000
})
// 请求拦截器 
Server.interceptors.request.use((config) => {
    // 不用加判断 当token 空的时候是不会添加到 header 头中去 
    store.dispatch({ type: 'SET_LOADING', payload: true })
    return config
}, (err) => Promise.reject(err))

// 响应拦截器 
Server.interceptors.response.use((response) => {
    store.dispatch({ type: 'SET_LOADING', payload: false })
    return response
}, (err) => Promise.reject(err))

// 抛出 Server 对象 
export default Server