import { createStore } from 'redux'

const reducer = (state, actions) => {
    if (!state) {
        state = {
            isLoding: false
        }
    }

    let status = actions.payload;
    
    switch (actions.type) {
        case "SET_LOADING":
            return {
                ...state,
                isLoding: status
            }    
        default:
            return {
                ...state
            }
            
    }
}

const store = createStore(reducer)

export{ store }