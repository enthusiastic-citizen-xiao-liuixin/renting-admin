import React from 'react';
import ReactDOM from 'react-dom/client';

import reportWebVitals from './reportWebVitals';
import { HashRouter as Router, Redirect, Route, Switch } from 'react-router-dom'

//样式 
import './index.css';
import './rem';

// 组件 
import App from './App';
import City from './component/city/City'
import Map from './component/map/Map';
import Detail from './component/detail/Detail';


// redux 
import { store } from './store/index.js'

const root = ReactDOM.createRoot(document.getElementById('root'));
// 路由 
const Routers = function () {
  return (
    <Router>
      <Switch>
        <Route path="/app" component={App}></Route>
        <Route path="/city" component={City}></Route>
        <Route path="/map" component={Map}></Route>
        <Route path="/detail/:id" component={Detail}></Route>
        <Redirect to="/app/home"></Redirect>
      </Switch>
    </Router>
  )
}

store.subscribe(() => {
  root.render(
    <Routers />
  );
})
// 页面第一次默认执行 
root.render(
  <Routers />
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
