import React, { useEffect, useState } from 'react'
import { NavBar, Popup, Tag, SpinLoading } from 'antd-mobile'
import { useHistory } from 'react-router-dom'
import { store } from '../../store/index'
import axios from '../../utils/request.js'
import './Map.scss'

export default function Maps() {

    let [isShow, setIsShow] = useState(false)
    let history = useHistory()
    // 房屋详细信息获取 
    let [housesInfo, setHousesInfo] = useState([])
    let [ loading, setLoading ] = useState(false)
    // 初始化调用 
    useEffect(() => {

        initMap()

    }, [])

    
    // 初始化地图 
    const initMap = () => {
        // 获取当前定位城市
        const { label, value } = JSON.parse(sessionStorage.getItem('hkzf_city'))
        // 初始化地图实例
        var map = new window.BMapGL.Map("container");

        // 创建地址解析器实例
        var myGeo = new window.BMapGL.Geocoder();
        // 将地址解析结果显示在地图上，并调整地图视野
        myGeo.getPoint(label, async (point) => {
            if (point) {
                //缩放
                map.centerAndZoom(point, 11);
                //添加标记
                map.addOverlay(new window.BMapGL.Marker(point))
                //添加控件
                map.addControl(new window.BMapGL.ScaleControl())
                map.addControl(new window.BMapGL.ZoomControl())
                renderOverLays(value, 11, map, point)
            } else {
                alert('您选择的地址没有解析到结果！');
            }
        }, label)
        map.addEventListener('movestart', () => {
            if (isShow) {
                setIsShow(false)
            }
        })

    }

    // 获取房屋详细信息 
    const getHousesInfoHandle = async (id) => {
        const { data } = await axios.get(`http://shiyansong.cn:8080/houses?cityId=${id}`)
        let result = data.body.list
        setHousesInfo(result)
    }

    // 跳转详情 
    const toDetail = async(item) =>{
        let { data } = await axios.get(`http://shiyansong.cn:8080/houses/${item.houseCode}`)
        window.sessionStorage.setItem('coord', JSON.stringify(data.body.coord))
        history.push({ pathname: `/detail/${item.houseCode}` });


    }



    // 熏染覆盖物 接收id参数获取区域房源数据 或覆盖类型以及下级地图的缩放级别
    const renderOverLays = async (id, zoom, map, point) => {
        const { data } = await axios.get(`http://shiyansong.cn:8080/area/map?id=${id}`)
        let result = data.body
        result.forEach(item => {
            // 创建覆盖物
            createOverlays(item, zoom, map, point)
        })
    }

    // 创建区, 镇覆盖物
    const createCircle = (item, zoom, map, point) => {
        const { coord: { longitude, latitude }, label: areaName, count, value } = item
        const areaPoint = new window.BMapGL.Point(longitude, latitude)

        //添加文本覆盖物
        const opts = {
            position: areaPoint,
            offset: new window.BMapGL.Size(30, -30)
        }
        //注意：设置setContent以后，第一个参数中设置的文本内容就失效了，因此直接清空即可
        const label = new window.BMapGL.Label('', opts)

        //设置房源覆盖物内容
        label.setContent(`
              <div class="contest-box"  key=${value}>
                  <p class="text-bxo">${areaName}</p>
                  <p  class="text-bxo">${count}套</p>
              </div>
            `)

        //设置样式
        const labelStyle = {
            cursor: 'pointer',
            border: '2px solid white',
            whiteSpace: 'nowrap',
            fontSize: '12px',
            textAlign: 'center',
            borderRadius: '50%',
            width: '65px',
            height: '65px',
            background: 'rgba(38, 191, 110, 0.9)'

        }
        label.setStyle(labelStyle)

        zoom += 2
        if (zoom >= 15) {
            zoom = 15
        }
        // 添加单击事件
        label.addEventListener('click', () => {
            // 调用 renderOverlays 方法，获取该区域下的房源数据
            renderOverLays(item.value, zoom, map, point)

            // 放大地图，以当前点击的覆盖物为中心放大地图
            map.centerAndZoom(areaPoint, zoom)

            // 解决清除覆盖物时，百度地图API的JS文件自身报错的问题
            setTimeout(() => {
                // 清除当前覆盖物信息
                map.clearOverlays()
            }, 0)
        })
        map.addOverlay(label)
    }
    // 创建房屋信息 方块 
    const createRect = (item, zoom, map, point) => {
        const { coord: { longitude, latitude }, label: areaName, count, value } = item
        const areaPoint = new window.BMapGL.Point(longitude, latitude)

        //添加文本覆盖物
        const opts = {
            position: areaPoint,
            offset: new window.BMapGL.Size(30, -30)
        }
        //注意：设置setContent以后，第一个参数中设置的文本内容就失效了，因此直接清空即可
        const label = new window.BMapGL.Label('', opts)

        //设置房源覆盖物内容
        label.setContent(`
              <div class="houses-box">
                    <p>
                        ${areaName} ${count}套
                    </p>
          
              </div>
            `)

        //设置样式
        const labelStyle = {
            border: 'none',
            cursor: 'pointer',
            whiteSpace: 'nowrap',
            fontSize: '12px',
            textAlign: 'center',
            height: '20px',
            background: 'rgba(38, 191, 110, 0.9)'

        }
        label.setStyle(labelStyle)

        zoom++
        // 添加单击事件
        label.addEventListener('click', () => {
            // 调用 renderOverlays 方法，获取该区域下的房源数据
            renderOverLays(item.value, zoom, map, point)

            // 放大地图，以当前点击的覆盖物为中心放大地图
            map.centerAndZoom(areaPoint, zoom)

            if (zoom === 16) {
                setIsShow(true)
                getHousesInfoHandle(item.value)
            }

        })
        map.addOverlay(label)
    }

    // 创建覆盖物 根据传入的数据调用对应的方法 创建覆盖物 
    const createOverlays = (item, zoom, map, point) => {
        switch (zoom) {
            case 11:
                createCircle(item, zoom, map, point)
                break;
            case 13:
                createCircle(item, zoom, map, point)
                break;
            case 15:
                createRect(item, zoom, map, point)
                break;
            default:
                break;
        }
    }   



    return (
        <div className='map' style={{ 'opacity':store.getState().isLoding ? '.5' : ''}}>

        {   store.getState().isLoding ? <SpinLoading color='primary' /> : "" }

            <NavBar onBack={() => { history.go(-1) }}>地图找房</NavBar>

            <div id='container' style={{ 'height': '799px' }}></div>

            <Popup
                visible={isShow}
                onMaskClick={() => {
                    setIsShow(false)
                }}
            >
                <div
                    style={{ height: '40vh', overflowY: 'scroll', padding: '20px' }}
                    className="house-box"
                >
                    <div className='header-box'>
                        <h2>房屋列表</h2>
                        <p>更多房源</p>
                    </div>

                    <div className='house-info-box'>
                        {

                            housesInfo.map(item => {
                                return (
                                    <ul onClick={() =>{ toDetail(item) }} className='house-info-item' key={item.houseCode}>
                                        <li>
                                            <img src={'http://shiyansong.cn:8080' + item.houseImg} alt="" />
                                        </li>
                                        <li>
                                            <h3>{item.title}</h3>
                                            <p>{item.desc}</p>
                                            <div>
                                                {
                                                    item.tags.map(item => <Tag color="success" style={{ 'margin': '3px' }} key={item}>{item}</Tag>)
                                                }
                                            </div>

                                            <h4>{item.price} 元/月</h4>
                                        </li>
                                    </ul>
                                )
                            })
                        }

                    </div>
                </div>
            </Popup>

        </div>
    )
}
