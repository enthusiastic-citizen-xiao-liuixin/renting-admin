import React from 'react'
import { Grid } from 'antd-mobile'
import { TruckOutline, StarOutline,FlagOutline,FileOutline  ,UserOutline, UserContactOutline} from 'antd-mobile-icons'
import './Me.scss'

export default function Me() {
    return (
        <div className='me'>
            <div>
                <img style={{ 'width': '100%' }} src="http://shiyansong.cn:8080/img/profile/bg.png" alt="" />
            </div>
            {/* 卡片区域 */}
            <div className='card'>
                <div className='head-portrait'>
                    <img src="http://shiyansong.cn:8080/img/profile/avatar.png" alt="" />
                </div>
                <p>游客</p>
                <button>去登录</button>
            </div>
            {/* 底部功能区域 */}
            <div style={{ 'height': '1.5rem' }}></div>
            <div>
                <Grid columns={3} gap={8}>
                    <Grid.Item>
                        <div className='grid-demo-item-block'>
                        <StarOutline fontSize={24} />
               
                            <h3>我的收藏</h3>
                        </div>
                    </Grid.Item>
                    <Grid.Item>
                        <div className='grid-demo-item-block'>
                        <TruckOutline fontSize={24} />
                            <h3>我的出租</h3>
                        </div>
                    </Grid.Item>
                    <Grid.Item>
                        <div className='grid-demo-item-block'>
                        <FileOutline  fontSize={24}/>
                            <h3>看房记录</h3>
                        </div>
                    </Grid.Item>
                    <Grid.Item>
                        <div className='grid-demo-item-block'>
                        <FlagOutline fontSize={24}/>
                            <h3>我为房主</h3>
                        </div>
                    </Grid.Item>
                    <Grid.Item>
                        <div className='grid-demo-item-block'>
                        <UserOutline fontSize={24}/>
                            <h3>个人资料</h3>
                        </div>
                    </Grid.Item>
                    <Grid.Item>
                        <div className='grid-demo-item-block'>
                        <UserContactOutline fontSize={24}/>
                            <h3>联系我们</h3>
                        </div>
                    </Grid.Item>
                </Grid>
            </div>

            <div className='footer' style={{ 'width': '90%', 'margin': '0 auto' }}>
                <img style={{ 'width': '100%' }} src="http://shiyansong.cn:8080/img/profile/join.png" alt="" />
            </div>
        </div>
    )
}
