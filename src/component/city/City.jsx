import React, { useEffect, useState } from 'react'
import { NavBar, IndexBar, List } from 'antd-mobile'
import axios from 'axios'
import { useHistory } from 'react-router-dom'

import './City.scss'


export default function () {
    const history = useHistory()
    useEffect(() => {
        getCityList()
    }, [])
    // 列表数据
    const [list, setList] = useState([])
    const formatCityData = (list) => {
        const cityList = {}
        //1. - 我们需要遍历 list 数组
        //2. - 获取到每一个城市的首字母
        //3. - 判断我们定义的数组中是否有这个分类，如果有，那么直接push数据进来，如果没有，添加这个分类
        //4. - 当城市列表数据按照首字母分好类了之后，还需要实现热门城市数据和定位城市数据
        //5. - 获取热门城市数据，添加到`cityList` 列表数据中，将索引数据添加到 `cityIndex` 索引数据中
        //6. - 获取当前城市数据，添加到`cityList` 列表数据中，将索引数据添加到 `cityIndex` 索引数据中
        list.forEach((item) => {
            let first = item.short.substr(0, 1).toUpperCase() // 字符串截取 
            if (cityList[first]) {
                // 如果进入if 代表有这个值，我们只需要直接push进去
                cityList[first].push(item)
            } else {
                // 如果进入else 代表没有这个值，我们初始化这个属性，并且把当前数据设置进去
                cityList[first] = [item]
            }
        })
        // 接下来我们需要把 cityList里面所有的key取出来，放在数组中，充当城市列表右侧的首字母导航条
        const cityIndex = Object.keys(cityList).sort()
        return { cityList, cityIndex }
    }
    //  获取数据 
    const getCityList = async () => {
        let { data: res } = await axios.get('http://shiyansong.cn:8080/area/city?level=1')
        let { cityList, cityIndex } = formatCityData(res.body)
        // 获取热门城市数据
        let { data: hotRes } = await axios.get('http://shiyansong.cn:8080/area/hot')
        // 将热门数据添加到 cityList
        cityList['热'] = hotRes.body
        // 将热门数据添加到 cityIndex
        cityIndex.unshift('热')
        let ls = []
        Object.entries(cityList).forEach(item => {
            ls.push({ key: item[0], child: item[1] })
        })
        // 按顺序排序
        ls.sort((first, second) => first.key.localeCompare(second.key))
        setList(ls)
    }
    // 城市跳转
    const clickCityHandle =(val) =>{
        history.go(-1)
        window.sessionStorage.setItem('hkzf_city', JSON.stringify(val))
    }

    return (
        <div className='city' >
            <NavBar onBack={() => { history.go(-1) }}>选择城市</NavBar>
            {/* 城市列表 */}
            <div style={{ height: window.innerHeight }}>
                <IndexBar>
                    {list.map(group => {
                        const { key, child } = group
                        return (
                            <IndexBar.Panel
                                index={key}
                                title={`标题${key}`}
                                key={`标题${key}`}
                                
                            >
                                <List>
                                    {child.map((item, index) => (
                                        <List.Item onClick={ () =>{ clickCityHandle(item) } } key={index}>{item.label}</List.Item>
                                    ))}
                                </List>
                            </IndexBar.Panel>
                        )
                    })}
                </IndexBar>
            </div>
        </div>
    )
}
