import React, { useEffect, useState } from 'react'
import { NavBar, Swiper, Tag, DotLoading, Divider, Button } from 'antd-mobile'
import { useHistory } from 'react-router-dom'
import axios from 'axios'
import './Detail.scss'



export default function Detail(props) {
    let history = useHistory()
    // 轮播数据 
    // 房屋详细信息 
    let [detailInfo, setDetailInfo] = useState({})
    // 咨询
    let [news, setNews] = useState([])

    // 咨询
    const getNewsHandle = async () => {
        let { data } = await axios.get("http://shiyansong.cn:8080/home/news")
        setNews(data.body)
    }
    const getDetailHandle = async () => {
        let { data } = await axios.get(`http://shiyansong.cn:8080/houses/${props.match.params.id}`)
        setDetailInfo(data.body)

    }


    // 初始化地图 
    const initMap = () => {
        var map = new window.BMapGL.Map("container");          // 创建地图实例 
        let { longitude, latitude } = JSON.parse(window.sessionStorage.getItem("coord"))
        const areaPoint = new window.BMapGL.Point(parseInt(longitude).toFixed(3), parseInt(latitude).toFixed(3))
        map.centerAndZoom(areaPoint, 13)

        var content = detailInfo?.community || '城市美林1区';
        var label = new window.BMapGL.Label(content, {       // 创建文本标注
            position: areaPoint,                             // 设置标注的地理位置
            offset: new window.BMapGL.Size(10, 20)           // 设置标注的偏移量
        })
        map.addOverlay(label);                               // 将标注添加到地图中
    }

    useEffect(() => {
        getDetailHandle();

        getNewsHandle()
    }, [])

    useEffect( () =>{
        initMap()
    },[detailInfo] )

    const items = detailInfo.houseImg?.map((item, index) => (
        <Swiper.Item key={index}>
            <div className='content'>
                <img style={{ 'height': '2.25rem' }} src={'http://shiyansong.cn:8080' + item} alt="图片丢失了,可能是数据出现的问题" />
            </div>
        </Swiper.Item>
    ))
    return (
        <div className='detail' >
            {/* 顶部导航  */}
            <NavBar onBack={() => { history.go(-1) }}>
                {detailInfo.community}
            </NavBar>
            {/* 轮播区域 */}
            {detailInfo.houseImg?.length > 0 && <Swiper loop autoplay >{items}</Swiper>}

            {/* 详情区域 */}
            <div className='context-box'>
                <p>{detailInfo.title}</p>
            </div>
            {/* 标签 */}
            <div className='tag-box'>
                {
                    detailInfo.tags?.map((item, index) => {
                        return (
                            <Tag color='success' key={index}> {item} </Tag>
                        )
                    })
                }
            </div>
            {/* 详细金额金额 */}

            <div className='info-box'>

                <dl>
                    <dt>{detailInfo.price}/月</dt>
                    <dd>租金</dd>
                </dl>
                <dl>
                    <dt>{detailInfo.roomType}</dt>
                    <dd>房型</dd>
                </dl>
                <dl>
                    <dt>{detailInfo.size}平米</dt>
                    <dd>面积</dd>
                </dl>

            </div>
            {/* 房屋朝向 */}

            <ul className='house-box'>
                <li>
                    <span>装修: </span>  精装
                </li>
                <li>
                    <span>朝向: </span>{detailInfo.oriented}
                </li>
                <li>
                    <span>楼层: </span> {detailInfo.floor}
                </li>
                <li>
                    <span>类型: </span> {detailInfo.tags}
                </li>
            </ul>
            <div style={{ "height": '0.1rem', 'backgroundColor': '#eee' }}></div>
            {/* 标题 */}
            <div style={{ "padding": ".1rem", 'fontSize': "0.13rem", "lineHeight": '.2rem' }}>
                <p>小区: {detailInfo.community}</p>
            </div>
            {/* 地图区域 */}
            <div id='container' style={{ 'height': '2rem' }}></div>
            {/* 房屋配套、概括 */}
            <div className='generalize-box'>
                <h3>房屋配套</h3>
                <Divider></Divider>
                <p>暂无数据</p>
            </div>
            <div style={{ "height": '0.1rem', 'backgroundColor': '#eee' }}></div>
            {/* 房源概括 */}
            <div className='generalize-item-box'>
                <div className='generalize-child'>
                    {/* 顶部 */}
                    <dl>
                        <dt><img src="http://shiyansong.cn:8080/img/avatar.png" alt="" /></dt>
                        <dd>
                            <p>王女士</p>
                            <label> 已认证房主</label>
                        </dd>
                        <p>
                        </p>
                    </dl>
                    <div>
                        <Button size='small' color='success' fill='outline'>
                            发消息
                        </Button>
                    </div>
                </div>
                <div className='generalize-content'>
                    <p>
                        【户型介绍】 房子户型很方正，标准的3室2厅2卫电梯6楼总高7楼，东南朝向，清水房，任意装修。 【出租原因】 这是业主空置的一套房产，现诚心出租出去，清水房等待有缘人。。。。 【小区介绍】 小区交通和生活非 常便利，屋内清水可任意装修，南北通透，双阳台，小区内有游泳池，幼儿园等设施，周 边配套设施齐泉，居家舒适。
                    </p>
                </div>


            </div>
            {/* 猜你喜欢 */}
            {/* 咨询 */}
            <div className='news'>
                <div className='news-title'>
                    <h4>最新资讯</h4>
                </div>

                {
                    news.map(item => {
                        return (
                            <dl className='news-item' key={item.id}>
                                <dt>
                                    <img src={item.imgSrc} alt="" />
                                </dt>
                                <dd>
                                    <p>{item.title}</p>
                                    <div>
                                        <p>{item.from}</p>
                                        <p>{item.date}</p>
                                    </div>
                                </dd>
                            </dl>
                        )
                    })
                }
            </div>
            
             {/* 底部导航 */}
            <div className='nav'>
                <Button style={{ 'width': '25%'}}> 收藏 </Button>
                <Button style={{ 'width': '25%' }}> 在线咨询 </Button>
                <Button style={{ 'width': '50%' }} shape='rectangular' color='success'>电话预约</Button>
            </div>


        </div>
    )
}
