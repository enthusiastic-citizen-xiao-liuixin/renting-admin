import React, { useEffect, useState } from 'react'
import { DownFill, EnvironmentOutline, SearchOutline } from 'antd-mobile-icons'
import { useHistory } from 'react-router-dom'

import { Swiper, DotLoading } from 'antd-mobile'
import axios from 'axios'
import './Home.scss'

export default function Home() {
    // 页面轮播图 
    let [swipers, setSwiper] = useState([])
    // 租房小组
    let [groups, setGroups] = useState([])
    // 咨询
    let [news, setNews] = useState([])

    const history = useHistory()

    useEffect(() => {
        cityInfo()
        // 轮播信息 
        getSwiperHandle()
        // 租房小组  
        getGroupsHandle()
        // 咨询
        getNewsHandle()

    }, [])


    const cityInfo = () => {
        if (!window.sessionStorage.getItem('hkzf_city')) {
            window.sessionStorage.setItem('hkzf_city', JSON.stringify({ "label": "上海", "value": "AREA|dbf46d32-7e76-1196" }))
        }

    }
    // 轮播图
    const getSwiperHandle = async () => {
        let { data } = await axios.get("http://shiyansong.cn:8080/home/swiper")
        setSwiper(data.body)
    }
    // 租房小组
    const getGroupsHandle = async () => {
        let { data } = await axios.get("http://shiyansong.cn:8080/home/groups")
        setGroups(data.body)
    }
    // 咨询
    const getNewsHandle = async () => {
        let { data } = await axios.get("http://shiyansong.cn:8080/home/news")
        setNews(data.body)
    }



    // 功能区域
    const actionData = [
        { id: 1, action: '整租', imgPath: './images/opt_01.png' },
        { id: 1, action: '合租', imgPath: './images/opt_02.png' },
        { id: 1, action: '地图找房', imgPath: './images/opt_03.png' },
        { id: 1, action: '去出租', imgPath: './images/opt_04.png' }
    ]
    // 轮播
    const items = swipers.map((item) => (
        <Swiper.Item key={item.id}>
            <div className='content'>
                <img src={item.imgSrc} alt="" />
            </div>
        </Swiper.Item>
    ))

    // 城市跳转 
    const toCityHandel = () => {
        history.push({ pathname: '/city' })
    }

    return (
        <div className='home'>
            {/* 轮播 */}
            {
                swipers.length > 0 && <Swiper loop autoplay >{items}</Swiper>
            }

            {/* 顶部搜索 */}

            <div className="location">
                <div className='search'>
                    <label onClick={toCityHandel}>
                        <span className="name">
                            {JSON.parse(window.sessionStorage.getItem('hkzf_city')) && JSON.parse(window.sessionStorage.getItem('hkzf_city')).label}
                        </span>
                        <DownFill style={{ 'color': '#aaa' }} />
                    </label>

                    <span> | </span>
                    <p> <SearchOutline /> &nbsp;请输入小区或地址</p>
                </div>
                <div>
                    <EnvironmentOutline onClick={() => { history.push({ pathname: '/map' }) }} fontSize={22} style={{ 'color': '#fff' }} />
                </div>
            </div>

            {/* 功能区域 */}
            <div className="action">
                {
                    actionData.map((item, index) => {
                        return (
                            <dl key={index}>
                                <dt>
                                    <img src={item.imgPath} alt="" />
                                </dt>
                                <dd>
                                    {item.action}
                                </dd>
                            </dl>
                        )
                    })
                }
            </div>
            {/* 租房小组 */}
            <div className='groups'>
                <ul className='groups-header'>
                    <li>租房小组</li>
                    <li style={{ 'color': '#aaa' }}>更多</li>
                </ul>
                {/* 内容主体区域 */}
                <div className='groups-content'>
                    {
                        groups.map(item => {
                            return (
                                <div className='groups-content-item' key={item.id}>
                                    <dd>
                                        <p style={{ 'fontWeight': 'bold' }}>{item.title}</p>
                                        <p style={{ 'color': '#aaa' }}>{item.desc}</p>
                                    </dd>
                                    <dl>
                                        <img src={item.imgSrc} alt="" />
                                    </dl>
                                </div>
                            )
                        })
                    }
                </div>
            </div>

            {/* 咨询 */}
            <div className='news'>
                <div className='news-title'>
                    <h4>最新资讯</h4>
                </div>

                {
                    news.map(item => {
                        return (
                            <dl className='news-item' key={item.id}>
                                <dt>
                                    <img src={item.imgSrc} alt="" />
                                </dt>
                                <dd>
                                    <p>{item.title}</p>
                                    <div>
                                        <p>{item.from}</p>
                                        <p>{item.date}</p>
                                    </div>
                                </dd>
                            </dl>
                        )
                    })
                }
            </div>

        </div>
    )
}
