import React, { useEffect, useState, useRef } from 'react'
import './Apartment.scss'
import { NavBar, Dropdown, Popup, Tag, DemoBlock, Selector, CascadePickerView, PickerView, Button } from 'antd-mobile'
import { useHistory } from 'react-router-dom'
import { DownFill, DownOutline, ArrowDownCircleOutline, EnvironmentOutline, SearchOutline } from 'antd-mobile-icons'
import axios from 'axios'


export default function Apartment() {
    // 路由跳转 
    let history = useHistory()
    // 初始化房源数据 
    let [apartmentList, setApartmentList] = useState([])
    let [areaAndSubway, setAreaAndSubway] = useState([])
    // 赛选方式 
    let [rentType, setRentType] = useState([])
    let [value, setValue] = useState([])
    // 赛选价格 
    let [price, setPrice] = useState([])

    let [popupState, setPopupState] = useState(true)

    // 筛选初始化数据 
    let [filterData, setFilterData] = useState([])

    let filterkeys = [{ title: "户型", type: "roomType" }, { title: "朝向", type: 'oriented' }, { title: "楼层", type: 'floor' }, { title: "房屋亮点", type: 'characteristic' }]
    // 清空
    let ref = useRef(null)
    // 初始化调用 
    useEffect(() => {
        getApartmentListHandle()
        getConditionHandle()

    }, [])

    let city = JSON.parse(window.sessionStorage.getItem('hkzf_city'))
    // 初始化调用 
    const getApartmentListHandle = async () => {
        let { data } = await axios.get(`http://shiyansong.cn:8080/houses?id=${city.value}`)
        setApartmentList(data.body.list)
    }

    // 跳转城市页面 
    const toCityHandel = () => {
        history.push({ pathname: '/city' })
    }

    // 获取房屋查询条件
    const getConditionHandle = async () => {
        let { data } = await axios.get(`http://shiyansong.cn:8080/houses/condition?id=${city.value}`)
        // setarea
        let { rentType, area, subway, price, roomType, oriented, floor, characteristic } = data.body
        console.log(data.body);
        setRentType([[...rentType]])
        setAreaAndSubway([area, subway])
        setPrice([[...price]])
        setFilterData([{ "roomType": roomType }, { "oriented": oriented }, { "floor": floor }, { "characteristic": characteristic }])
    }
    // 跳转房屋详情 
    const clickHousesInfo = async(id) => {
        let { data } = await axios.get(`http://shiyansong.cn:8080/houses/${id}`)
        window.sessionStorage.setItem('coord', JSON.stringify(data.body.coord))
        history.push({ pathname: `/detail/${id}` });

     
    }

    return (
        <div className='apartment'>
            <NavBar onBack={() => { history.go(-1) }}>
                <div className='search'>
                    <label onClick={toCityHandel}>
                        <span className="name">
                            {JSON.parse(window.sessionStorage.getItem('hkzf_city')) && JSON.parse(window.sessionStorage.getItem('hkzf_city')).label}
                        </span>
                        &nbsp;
                        <DownFill fontSize={12} style={{ 'color': '#aaa' }} />
                        &nbsp;
                    </label>

                    <span> | </span>
                    &nbsp;
                    <p> <SearchOutline /> &nbsp;请输入小区或地址</p>
                </div>
                <EnvironmentOutline fontSize={18} onClick={() => { history.push({ pathname: '/map' }) }} style={{ 'color': 'green' }} />
            </NavBar>
            {/* 导航过滤 */}
            <div className='filter-box'>
                <Dropdown arrow={<DownOutline />} ref={ref}>
                    <Dropdown.Item key='sorter' title='区域' style={{ "fontWeight": 'bolder' }}>
                        <div>
                            <CascadePickerView options={areaAndSubway}
                                onChange={val => {
                                    setValue(val)
                                    console.log('onChange', val)
                                }}
                            />

                        </div>
                        <div style={{ "lineHeight": '0.7rem' }}>
                            <Button style={{ 'color': 'rgb(33, 185, 122)', 'width': '40%' }}>取消</Button>
                            <Button style={{ 'width': '60%', 'backgroundColor': 'rgb(33, 185, 122)', 'color': 'white' }}>确定</Button>
                        </div>
                    </Dropdown.Item>
                    <Dropdown.Item
                        key='bizop'
                        title='方式'
                        arrow={<ArrowDownCircleOutline />}
                    >
                        <div style={{ padding: 12 }}>

                            {rentType && <PickerView columns={rentType} mouseWheel={true} />}
                            <div style={{ "lineHeight": '0.7rem' }}>
                                <Button style={{ 'color': 'rgb(33, 185, 122)', 'width': '40%' }}>取消</Button>
                                <Button style={{ 'width': '60%', 'backgroundColor': 'rgb(33, 185, 122)', 'color': 'white' }}>确定</Button>
                            </div>
                        </div>
                    </Dropdown.Item>
                    <Dropdown.Item key='more' title='租金' style={{ "fontWeight": 'bolder' }}>
                        <div style={{ padding: 12 }}>

                            {price && <PickerView columns={price} mouseWheel={true} />}
                            <div style={{ "lineHeight": '0.7rem' }}>
                                <Button style={{ 'color': 'rgb(33, 185, 122)', 'width': '40%' }}>取消</Button>
                                <Button style={{ 'width': '60%', 'backgroundColor': 'rgb(33, 185, 122)', 'color': 'white' }}>确定</Button>
                            </div>
                        </div>
                    </Dropdown.Item>

                    <Dropdown.Item destroyOnClose key='filter' title='筛选' style={{ "fontWeight": 'bolder' }} >

                        <Popup
                            closeOnMaskClick
                            visible={popupState}
                            // onMaskClick={() => {
                            //     setPopupState(!popupState)
                            // }}
                            position='right'
                            style={{ "height": '120%', 'zIndex': 10000 }}
                            bodyStyle={{ width: '80vw' }}
                        >
                            {/* 内容区域 */}
                            <div style={{ 'padding': '.1rem' }}>
                                {

                                    filterData.map((item, index) => {
                                        return (
                                            <div key={index}>

                                                <p style={{ 'fontSize': '0.15rem', 'lineHeight': '.6rem' }}>{filterkeys[index].title}</p>

                                                <Selector
                                                    options={item[filterkeys[index].type]}
                                                    // defaultValue={['2', '3']}
                                                    multiple={true}
                                                    onChange={(arr, extend) => console.log(arr, extend.items)}
                                                />
                                            </div>

                                        )

                                    })
                                }
                                {/* <DemoBlock title='多选'>
                                   
                                </DemoBlock> */}
                            </div>
                            <div style={{ "lineHeight": '0.7rem' }}>
                                <Button
                                    onClick={() => {
                                        ref.current?.close()
                                    }}
                                    style={{ 'color': 'rgb(33, 185, 122)', 'width': '40%' }}>取消</Button>
                                <Button
                                    onClick={() => {
                                        ref.current?.close()
                                    }}
                                    style={{ 'width': '60%', 'backgroundColor': 'rgb(33, 185, 122)', 'color': 'white' }}>确定</Button>
                            </div>
                        </Popup>




                    </Dropdown.Item>
                </Dropdown>
            </div>
            {/* 主体区域 */}
            <div className='body'>
                {
                    apartmentList.map(item => {
                        return (
                            <dl key={item.title} onClick={() => { clickHousesInfo(item.houseCode) }}>
                                <dt>
                                    <img style={{ "height": '100%' }} src={"http://shiyansong.cn:8080" + item.houseImg} alt="" />
                                </dt>
                                <dd>
                                    <h3>{item.title}</h3>
                                    <p>{item.desc}</p>
                                    <div>
                                        <Tag>
                                            {item.tags}
                                        </Tag>

                                    </div>
                                    <span>
                                        <b>
                                            {item.price}
                                        </b>
                                        /元每月
                                    </span>
                                </dd>
                            </dl>
                        )
                    })
                }
            </div>
        </div>
    )
}
