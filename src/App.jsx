import React from 'react'
import { Route,useHistory,useLocation } from 'react-router-dom'
import { TabBar } from 'antd-mobile'
import './App.scss'
import { AppOutline, MessageOutline, UnorderedListOutline, UserOutline } from 'antd-mobile-icons'

import Home from './component/home/Home'
import Me from './component/me/Me'
import Apartment from './component/apartment/Apartment'


function App() {
    const history = useHistory()
    const location = useLocation()
    const { pathname } = location
    const setRouteActive = (value) => {
        history.push(value)
    }
    // 底部导航选项
    const tabs = [
        {
            key: '/app/home',
            title: '首页',
            icon: <AppOutline />,
        },
        {
            key: '/app/apartment',
            title: '找房',
            icon: <UnorderedListOutline />,
        },
        {
            key: '/app/message',
            title: '资讯',
            icon: <MessageOutline />,
        },
        {
            key: '/app/me',
            title: '我的',
            icon: <UserOutline />,
        },
    ]
    return (
        <div className="App">
            {/* 页面配置 */}
            <div>
                <Route path="/app/home" component={ Home }></Route>
                <Route path="/app/me" component={ Me }></Route>    
                <Route path='/app/apartment' component={ Apartment }></Route>        
            </div>
            {/* 底部导航 */}
            <TabBar className='nav' activeKey={pathname} onChange={value => setRouteActive(value)}>
                {
                    tabs.map(item => (<TabBar.Item key={item.key} icon={item.icon} title={item.title} />))
                }
            </TabBar>
        </div>
    );
}

export default App;
